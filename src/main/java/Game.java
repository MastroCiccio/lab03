import java.util.Arrays;
class Game {
    private int score;
    private int roll;
    private int[] frames;
    private boolean spare;
    private boolean strike;

    public Game() {
        this.score = 0;
        this.roll = 0;
        this.frames = new int[10];
        this.spare = false;
        this.strike = false;
    }

    //TODO: refactoring needed, terrible implementation
    public void roll(int pins) {
        this.score += pins;
        this.roll++;

        if(this.strike) {
            if(this.roll%2 == 0) {
                frames[this.roll/2-2] += this.score;
                frames[this.roll/2-1] = this.score;
                this.strike = false;
                this.score = 0;
                return;
            }
        }

        if(this.spare) {
            frames[this.roll/2-1] += pins;
            this.spare = false;
        }

        if(this.roll%2 == 0) {
            frames[this.roll/2-1] = this.score;

            if(this.score == 10) this.spare=true;

            this.score = 0;
        }

        if(this.roll%2 == 1 && pins == 10) {
            frames[this.roll/2] = this.score;
            this.strike = true;
            this.roll++;
            this.score = 0;
        }

    }

    public int score() {
        System.out.println(Arrays.toString(this.frames));
        int score = 0;
        for(int frame_score: frames) score+=frame_score;

        return score;
    }
}
